package com.pksheldon4.demo.testingwebfluxcontrollers;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.bson.Document;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@AllArgsConstructor
public class SampleRestController {

    SampleService sampleService;

    @GetMapping("/sample")
    public Mono<List<Document>> sampleData() {
        return sampleService.sampleData().log();
    }

}
