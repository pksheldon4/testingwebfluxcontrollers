package com.pksheldon4.demo.testingwebfluxcontrollers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class SampleControllerAdvice {

    @ExceptionHandler({SampleException.class})
    public ResponseEntity<String> handleSampleException(SampleException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

}