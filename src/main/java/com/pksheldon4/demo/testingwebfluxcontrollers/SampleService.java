package com.pksheldon4.demo.testingwebfluxcontrollers;

import org.bson.Document;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.*;

@Service
public class SampleService {

    public Mono<List<Document>> sampleData() {
        List<Document> documents = new ArrayList<>();
        Document doc1 = new Document();
        doc1.put("Test Field", "Test Value");
        doc1.put("status", "SUCCESS");
        documents.add(doc1);
        return Mono.just(documents);
    }
}
