package com.pksheldon4.demo.testingwebfluxcontrollers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestingWebFluxControllersApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestingWebFluxControllersApplication.class, args);
    }

}
