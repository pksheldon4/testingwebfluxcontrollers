package com.pksheldon4.demo.testingwebfluxcontrollers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class SampleRestControllerTest {

    WebTestClient client;
    SampleService sampleService;

    @BeforeEach
    void setUp() {
        sampleService = mock(SampleService.class);
        client = WebTestClient.bindToController(new SampleRestController((sampleService)))
                .controllerAdvice(new SampleControllerAdvice())
                .configureClient()
                .build();
    }

    @Test
    public void sampleData_success() throws Exception {
        when(sampleService.sampleData()).thenCallRealMethod();
        client.get()
                .uri("/sample")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("[*].status")
                .isEqualTo("SUCCESS");
    }

    @Test
    public void sampleData_throwsSampleException() throws Exception {
        when(sampleService.sampleData()).thenThrow(SampleException.class);
        client.get()
                .uri("/sample")
                .exchange()
                .expectStatus().is4xxClientError();

    }

    @Test
    public void sampleData_throwsException() throws Exception {
        when(sampleService.sampleData()).thenThrow(NullPointerException.class);
        client.get()
                .uri("/sample")
                .exchange()
                .expectStatus().is5xxServerError();

    }
}